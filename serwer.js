/*jshint globalstrict: true, devel: true, node: true */
'use strict';

var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var baza = require('./db/books');
var fs = require('fs');

app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    var genres = baza().distinct("genre");
    res.render('index.ejs', {genres: genres});
});

app.get('/:gen', function (req, res) {
    var genres = baza().distinct("genre");
    var books = baza({genre: req.params.gen}).select("title", "author");
    var genre = req.params.gen;
    res.render('index.ejs', {genres: genres, books: books, genre: genre});
});

app.post('/:gen', function (req, res) {
    var newAuthor=req.body.author;
    var newTitle=req.body.title;
    var newLogin=req.body.login;
    var newPassword=req.body.password;
    var genre = req.params.gen;
    console.log(newAuthor, newTitle);
    if(newLogin == "admin" && newPassword == "nimda"){
    baza.insert({"title":newTitle, "author":newAuthor, "genre": genre});
    
    var genres = baza().distinct("genre");
    var books = baza({genre: genre}).select("title", "author");
    res.render('index.ejs', {genres: genres, books: books, genre: genre});
}
else{
   var genres = baza().distinct("genre");
    var books = baza({genre: genre}).select("title", "author");
    res.render('index.ejs', {genres: genres, books: books, genre: genre});  
    
}
});


app.listen(3000, function () {
    console.log('Serwer działa na porcie 3000');
});


process.on('SIGINT',function(){
  console.log('\nshutting down');
  var zapis = "var TAFFY = require('taffy');\n\nvar books = TAFFY(";
zapis += baza().stringify();
zapis += ");\n\nmodule.exports = books;";
  fs.writeFile('./db/books.txt', zapis, function (err, data) {
  if (err) console.log(err);
  process.exit();
});
});